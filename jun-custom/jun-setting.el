(setq ring-bell-function 'ignore)

(require 'exec-path-from-shell)
    (when (memq window-system '(mac ns))
    (exec-path-from-shell-initialize))

;; disable flymake popup dialogbox
(setq flymake-gui-warnings-enabled nil)

;; to control line space
(setq-default line-spacing 5)

;; to save the last session
(desktop-save-mode 1)

;; disable backup
(setq backup-inhibited t)

;; disable auto save
(setq auto-save-default nil)

;; Disable the Toolbar
(tool-bar-mode -1)

;; deleting tailing whitespace before saving
(add-hook 'before-save-hook 'delete-trailing-whitespace)

;; disable cursor blinking
(blink-cursor-mode 0)

;; smart beginning of line
(defun smart-beginning-of-line ()
  "Move point to first non-whitespace character or beginning-of-line.
Move point to the first non-whitespace character on this line.
If point was already at that position, move point to beginning of line."
  (interactive)
  (let ((oldpos (point)))
    (back-to-indentation)
    (and (= oldpos (point))
         (beginning-of-line))))
(global-set-key [home] 'smart-beginning-of-line)
(global-set-key "\C-a" 'smart-beginning-of-line)

;; Theme
(require 'color-theme)
(color-theme-initialize)
(require 'greymatters-theme)
;;(load-theme 'leuven)
;;(load-theme 'zenburn t)
;;(set-cursor-color "firebrick")
;;(load-theme 'solarized-light t)
;;(load-theme 'solarized-dark t)
;;(color-theme-bharadwaj)
;;(color-theme-snowish)

;; Font
(cond
 ((string-equal system-type "windows-nt") ; Microsoft Windows
  (progn
	  ;(set-frame-font "Inconsolata 12" nil t)
		;(set-frame-font "Courier New 12" nil t)
		(set-frame-font "Consolas 12" nil t)
    ;(set-frame-font "DejaVu Sans Mono 12" nil t)
		;(set-frame-font "Source Code Pro 12" nil t)
		;(set-frame-font "Ubuntu Mono 12" nil t)
		;(set-frame-font "Droid Sans Mono 12" nil t)
		;(set-frame-font "Bitstream Vera Sans Mono 10" nil t)
		)
	)
 ((string-equal system-type "darwin")   ; Mac OS X
  (progn
    (set-frame-font "Inconsolata 18" nil t)
    ;;(set-frame-font "Consolas 15" nil t)
    ;;(set-frame-font "DejaVu Sans Mono 14" nil t)
    ;;(set-frame-font "Source Code Pro 14" nil t)
		;;(set-frame-font "Ubuntu Mono 14" nil t)

		;; Use font faces in current buffer
		(defun my-buffer-face-mode-iess ()
			"Sets a fixed width (monospace) font in current buffer"
			(interactive)
			(setq buffer-face-mode-face '(:family "Inconsolata" :height 150))
			(buffer-face-mode))
    )
  )
 ((string-equal system-type "gnu/linux") ; linux
  (progn
    (set-frame-font "Inconsolata 17" nil t)
    ;;(set-face-attribute 'default nil :height 140)
    ;;(set-frame-font "Source Code Pro Semibold-13" nil t)
		;; Use font faces in current buffer
		(defun my-buffer-face-mode-iess ()
			"Sets a fixed width (monospace) font in current buffer"
			(interactive)
			(setq buffer-face-mode-face '(:family "Inconsolata" :height 160))
			(buffer-face-mode))
    )
  )
 )

;; Don't show startup screen
(setq inhibit-startup-screen t)

;; Scrolling behavior
(setq redisplay-dont-pause t
      scroll-margin 1
      scroll-step 1
      scroll-conservatively 10000
      scroll-preserve-screen-position 1)
(setq mouse-wheel-follow-mouse 't)
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1)))

;; dired sort case insensitive
(require 'dired-sort-menu+)

;; use shift + arrow keys to switch between visible buffers
(require 'framemove)
(windmove-default-keybindings)
(setq windmove-wrap-around t)
(setq framemove-hook-into-windmove t)

;; open buffer list in the same window
;; invoke buffer-menu rather than list-buffers
(global-set-key "\C-x\C-b" 'buffer-menu)

;; automatically save buffers associated with files on buffer switch
;; and on windows switch
(defadvice switch-to-buffer (before save-buffer-now activate)
  (when buffer-file-name (save-buffer)))
(defadvice other-window (before other-window-now activate)
  (when buffer-file-name (save-buffer)))
(defadvice windmove-up (before other-window-now activate)
  (when buffer-file-name (save-buffer)))
(defadvice windmove-down (before other-window-now activate)
  (when buffer-file-name (save-buffer)))
(defadvice windmove-left (before other-window-now activate)
  (when buffer-file-name (save-buffer)))
(defadvice windmove-right (before other-window-now activate)
  (when buffer-file-name (save-buffer)))

;; auto-cleaning buffer
(require 'midnight)
;; clear 5 days unused buffers
(setq clean-buffer-list-delay-general 5)

;; show-paren-mode
(setq show-paren-delay 0)
(show-paren-mode 1)
;; show the line of matching paren in the minibuffer
(defadvice show-paren-function
  (after show-matching-paren-offscreen activate)
  "If the matching paren is offscreen, show the matching line in the
        echo area. Has no effect if the character before point is not of
        the syntax class ')'."
  (interactive)
  (let* ((cb (char-before (point)))
	 (matching-text (and cb
			     (char-equal (char-syntax cb) ?\) )
			     (blink-matching-open))))
    (when matching-text (message matching-text))))

;; undo-tree
(require 'undo-tree)
(global-undo-tree-mode)

;;----- Helm ----------------
(require 'helm)
(require 'helm-config)

;; The default "C-x c" is quite close to "C-x C-c", which quits Emacs.
;; Changed to "C-c h". Note: We must set "C-c h" globally, because we
;; cannot change `helm-command-prefix-key' once `helm-config' is loaded.
(global-set-key (kbd "C-c h") 'helm-command-prefix)
(global-unset-key (kbd "C-x c"))

(define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action) ; rebind tab to run persistent action
(define-key helm-map (kbd "C-i") 'helm-execute-persistent-action) ; make TAB works in terminal
(define-key helm-map (kbd "C-z")  'helm-select-action) ; list actions using C-z

(when (executable-find "curl")
  (setq helm-google-suggest-use-curl-p t))

(setq helm-split-window-in-side-p           t ; open helm buffer inside current window, not occupy whole other window
      helm-move-to-line-cycle-in-source     t ; move to end or beginning of source when reaching top or bottom of source.
      helm-ff-search-library-in-sexp        t ; search for library in `require' and `declare-function' sexp.
      helm-scroll-amount                    8 ; scroll 8 lines other window using M-<next>/M-<prior>
      helm-ff-file-name-history-use-recentf t)

(helm-mode 1)
