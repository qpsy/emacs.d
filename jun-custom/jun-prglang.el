;;; yasnippet
;;; should be loaded before auto complete so that they can work together
(require 'yasnippet)
(yas-global-mode 1)

(require 'auto-complete-config)
;;;(add-to-list 'ac-dictionary-directories "~/.emacs.d/ac-dict")
(ac-config-default)

;;; set the trigger key so that it can work together with yasnippet on tab key,
;;; if the word exists in yasnippet, pressing tab will cause yasnippet to
;;; activate, otherwise, auto-complete will
(ac-set-trigger-key "TAB")
(ac-set-trigger-key "<tab>")


;;----------   ess   ------------------
(require 'ess-site)
(require 'ess-eldoc)
(ac-set-trigger-key "TAB")
(require 'helm-R)

(setq ess-ask-for-ess-directory nil)
(cond
 ((string-equal system-type "darwin")   ; Mac OS X
  (progn
    (setq inferior-R-program-name "/usr/local/bin/R")
		;;iESS font
		(add-hook 'inferior-ess-mode-hook 'my-buffer-face-mode-iess)
    )
  )
 ((string-equal system-type "gnu/linux") ; linux
  (progn
    (setq inferior-R-program-name "/usr/bin/R")
		;;iESS font
		(add-hook 'inferior-ess-mode-hook 'my-buffer-face-mode-iess)
	 ) ) )
(setq ess-local-process-name "R")
(setq ansi-color-for-comint-mode 'filter)
(setq comint-move-point-for-output t)
(setq ess-eval-visibly-p nil)

;; ESS will not print the evaluated commands, also speeds up the evaluation
;;(setq ess-eval-visibly nil)
(setq ess-eval-visibly-p nil)

(setq ess-ask-for-ess-directory nil)
(setq ansi-color-for-comint-mode 'filter)

;; place cursor on bottom line
(setq comint-scroll-to-bottom-on-input t)
(setq comint-scroll-to-bottom-on-output t)
(setq comint-move-point-for-output t)

;; disable command history
(setq ess-history-file nil)

;; to pup-up help on its own frame
(setq ess-help-own-frame t)

;; help frame size
(add-to-list 'ess-help-frame-alist '(width . 75))
(add-to-list 'ess-help-frame-alist '(height . 25))

;; several help windows
(setq ess-help-reuse-window nil)

(setq ess-eval-visibly-p nil)

;; indent-level 2
;;(setq ess-indent-level 2)
(defun myindent-ess-hook ()
  (setq ess-indent-level 2))
(add-hook 'ess-mode-hook 'myindent-ess-hook)

;; auto-complete ESS buffers only
;; (setq ess-use-auto-complete t)

;; keybinding quick help (default: C-?)
(define-key ac-completing-map (kbd "M-h") 'ac-quick-help)

;; to bind ac-complete to tab
(define-key ac-completing-map [tab] 'ac-complete)

;; to remove the binding of ac-complete to the return key
(define-key ac-completing-map [return] nil)
(define-key ac-completing-map "\r" nil)

;; prevent arrow key to open autocomplete menu
(define-key ac-completing-map [down] nil)
(define-key ac-completing-map [up] nil)

;; ;; binds M-n, and M-p for navigation in the completion menu
(define-key ac-completing-map "\M-n" nil) ;; was ac-next
(define-key ac-completing-map "\M-p" nil) ;; was ac-previous
(define-key ac-completing-map "\M-," 'ac-next)
(define-key ac-completing-map "\M-k" 'ac-previous)

;; AC color setting
(set-face-attribute 'ac-candidate-face nil   :background "#00222c" :foreground "light gray")
(set-face-attribute 'ac-selection-face nil   :background "SteelBlue4" :foreground "white")
(set-face-attribute 'popup-tip-face    nil   :background "#003A4E" :foreground "light gray")

;; Misc customization of auto-complete
(setq
      ;; ac-auto-show-menu 1
      ;; ac-candidate-limit nil
      ;; ac-delay 0.1
      ;; ac-disable-faces (quote (font-lock-comment-face font-lock-doc-face))
      ;; ac-ignore-case 'smart
      ;; ac-menu-height 10
      ;; ac-quick-help-delay 1.5
      ;; ac-quick-help-prefer-pos-tip t
      ac-use-quick-help nil
)

;; disable auto-indentation of comment #
(setq ess-fancy-comments nil)

;; set function argument suffix with "=", defalut " = "
(setq ess-ac-R-argument-suffix "=")

;; disable auto-indent for single comment #
(add-hook 'ess-mode-hook
          (lambda ()
            (local-set-key (kbd "RET") 'newline)))


;;--------   Python   ------------
;(elpy-enable)
;(elpy-use-ipython)

(setq python-indent-offset 2)

;; run python3
(defun run-python3 () (interactive) (run-python "python3"))
(global-set-key [f5] 'run-python3)


;;----------  Haskell  ---------------
(require 'haskell-mode)
(add-hook 'haskell-mode-hook 'turn-on-haskell-indentation)
(add-hook 'haskell-mode-hook 'turn-on-haskell-doc-mode)

;; indentation : choose one of the three
;;(add-hook 'haskell-mode-hook 'turn-on-haskell-indentation)
(add-hook 'haskell-mode-hook 'turn-on-haskell-indent)
;;(add-hook 'haskell-mode-hook 'turn-on-haskell-simple-indent)

;; 2 spaces indentation
(setq haskell-indent-offset 2)

;; align rules
(require 'align)
(add-to-list 'align-rules-list
	     '(haskell-types
	       (regexp . "\\(\\s-+\\)\\(::\\|∷\\)\\s-+")
	       (modes quote (haskell-mode literate-haskell-mode))))
(add-to-list 'align-rules-list
	     '(haskell-assignment
	       (regexp . "\\(\\s-+\\)=\\s-+")
	       (modes quote (haskell-mode literate-haskell-mode))))
(add-to-list 'align-rules-list
	     '(haskell-arrows
	       (regexp . "\\(\\s-+\\)\\(->\\|→\\)\\s-+")
	       (modes quote (haskell-mode literate-haskell-mode))))
(add-to-list 'align-rules-list
	     '(haskell-left-arrows
	       (regexp . "\\(\\s-+\\)\\(<-\\|←\\)\\s-+")
	       (modes quote (haskell-mode literate-haskell-mode))))

;;------- Auctex  -------------------------
;; AucTeX
(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq-default TeX-master nil)
(add-hook 'LaTeX-mode-hook 'visual-line-mode)
(add-hook 'LaTeX-mode-hook 'flyspell-mode)
(add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)
(add-hook 'LaTeX-mode-hook 'turn-on-reftex)
(setq reftex-plug-into-AUCTeX t)
(setq TeX-PDF-mode t)

;; Use Skim as viewer, enable source <-> PDF sync
;; make latexmk available via C-c C-c
;; Note: SyncTeX is setup via ~/.latexmkrc (see below)
(add-hook 'LaTeX-mode-hook (lambda ()
  (push
    '("latexmk" "latexmk -pdf %s" TeX-run-TeX nil t
      :help "Run latexmk on file")
    TeX-command-list)))
(add-hook 'TeX-mode-hook '(lambda () (setq TeX-command-default "latexmk")))

;; use Skim as default pdf viewer
;; Skim's displayline is used for forward search (from .tex to .pdf)
;; option -b highlights the current line; option -g opens Skim in the background
(setq TeX-view-program-selection '((output-pdf "PDF Viewer")))
(cond
 ((string-equal system-type "windows-nt") ; Microsoft Windows

  )
 ((string-equal system-type "darwin")   ; Mac OS X
  (progn
    (setq TeX-view-program-list
	  '(("PDF Viewer" "/Applications/Skim.app/Contents/SharedSupport/displayline -b -g %n %o %b")))
    )
  )
 ((string-equal system-type "gnu/linux") ; linux

  )
 )
