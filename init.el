;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

;; check OS type
(cond
 ((string-equal system-type "windows-nt") ; Microsoft Windows
  (progn
    (message "Microsoft Windows") )
  )
 ((string-equal system-type "darwin")   ; Mac OS X
  (progn
    (message "Mac OS X")
    )
  )
 ((string-equal system-type "gnu/linux") ; linux
  (progn
    (message "Linux") )
  )
 )

;; Package Manager
;; See ~Cask~ file for its configuration
;; https://github.com/cask/cask
(cond
 ((string-equal system-type "windows-nt") ; Microsoft Windows
  (progn
    (require 'cask "~/gits/cask/cask.el") )
  )
 ((string-equal system-type "darwin")   ; Mac OS X
  (progn
    (require 'cask "~/gits/cask/cask.el")
    )
  )
 ((string-equal system-type "gnu/linux") ; linux
  (progn
    (require 'cask "~/gits/cask/cask.el") )
  )
 )

(cask-initialize)

;; Keeps ~Cask~ file in sync with the packages
;; that you install/uninstall via ~M-x list-packages~
;; https://github.com/rdallasgray/pallet
(require 'pallet)

;; add paths and load files
(add-to-list 'load-path "~/.emacs.d/jun-custom")
(load "jun-setting.el")
(load "jun-prglang.el")
;;(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
;; '(package-selected-packages
;;	 (quote
;;		(dash package-build org yasnippet undo-tree pallet haskell-mode framemove flx-ido exec-path-from-shell ess ein dired-sort-menu+ color-theme auto-complete-clang))))
